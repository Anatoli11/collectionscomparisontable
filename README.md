# CollectionsComparisonTable

| Collection | Indexed lookup | Keyed lookup | Value lookup | Addition |  Removal |  Memory |
| -------- | -------- | --------| --------|  -------- |  -------- |  -------- |
| **Списки** | | | | | | |
| `T[]` | Yes | - | Yes | No | No |  |
| `List<T>` | Yes | - | Yes | Yes | O*(1) | |
|`LinkedList<T>`  | No | - | Yes | O(1) | O(1) |  |
|`Collection<T>`  |  |  |  |  |  |  |
|`BindingList<T>`  |  |  |  |  |  |  |
|`ObservableCollection<T>`  |  |  |  |  |  |  |
|`KeyCollection<TKey, TItem>`  |  |  |  |  |  |  |
|`ReadOnlyCollection<T>`  |  |  |  |  |  |  |
|`ReadOnlyObservableCollection<T>` |  |  |  |  |  |  |
**Словари** | | | | | | |  
|`Dictionary<TKey, TValue>`   | No |  |  |  |  |  |
|`SortedList<TKey, TValue>`   | Yes |  |  |  |  |  |
|`SortedDictionary<TKey, TValue>`  | No |  |  |  |  |  |
|`SortedDictionary<TKey,TValue>`.   | No |  |  |  |  |  |  
`ReadOnlyDictionary<TKey, TValue> `   |  |  |  |  |  |  |
**Множества** | | | | | | | 
|`HashSet<T>`  | No |  |  |  |  |  |
|`SortedSet<T>`   |  |  |  |  |  |  |
| **Очередь, стек** | | | | | | | 
|`Queue<T>`  |  |  |  |  |  |  |
|`Stack<T>`  |  |  |  |  |  |  |
* `*` If ... .
* `**`If ... .


|Collection | Underlying structure | Lookup strategy | Ordering | Contiguous storage | Data access | Exposes Key & Value collection | 
| -------- | -------- | --------| --------|  -------- |  -------- |  -------- | 
**Списки** | | | | | | |  
|`T[]` | array |  | | | | |   
|`List<T>` | array |  | | | | |   
|`LinkedList<T>` | list |  | | | | |   
|`Collection<T>` | |  | | | | |   
|`BindingList<T>` | |  | | | | |   
|`ObservableCollection<T>`  | |  | | | | |   
|`KeyCollection<TKey, TItem>`  | |  | | | | |   
|`ReadOnlyCollection<T>` | |  | | | | |   
|`ReadOnlyObservableCollection<T>`  | |  | | | | | 
|**Словари** | | | | | | | 
|`Dictionary<TKey, TValue>` | Hash Table |  | | | | |    
|`SortedList<TKey, TValue>`  | 2xArray |  | | | | |   
|`SortedDictionary<TKey, TValue>`  | Red/Black Tree |  | | | | |   
|`ReadOnlyDictionary<TKey, TValue>`  | |  | | | | |   
|**Множества** | | | | | | | 
|`HashSet<T>` | Hash Table |  | | | | |   
|`SortedSet<T>`  | Red/Black Tree |  | | | | |   
|**Очередь, стек** | | | | | | | 
|`Queue<T>` | Queue |  | | | | |   
|`Stack<T>` | Stack |  | | | | |   

